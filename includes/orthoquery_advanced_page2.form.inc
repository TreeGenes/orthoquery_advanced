<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//User Interface 

error_reporting(E_ALL);

global $user;
if (!$user->uid) {
	$loginPage = $GLOBALS['base_url'].'/user';
	drupal_set_message("In order to execute a search, you must be logged into the database. "
			. "Click <a href=\"$loginPage\">here</a> to log in.",'error');
}

$form['Overview'] = array(
	'#type'         => 'fieldset',
	'#title'        => 'OrthoQuery Overview',
	'#collapsible'  => FALSE,
	'#description'  => t('Orthofinder is developed for comparative genomic analysis using orthogroups. Users can discover novel orthogroups (i.e. gene families) by uploading a transcriptome or a proteome and selecting species in the TreeGenes database for a comparative genomics analysis. Otherwise, users can upload a small set of protein sequences and determine which gene families these sequences belong by selecting species that reside in TreeGenes.'),
	'#suffix'       => '<br /><br /><h3>Orthogroup Analysis</h3>'
);

$form['debug'] = array(
	'#type'			=> 'checkbox',
	'#title'		=> 'Debug',
);

$form['do_only_test_dataset'] = array(
	'#type'			=> 'checkbox',
	'#title'		=> 'Perform only test dataset',
);


 
$form['enable_submission'] = array(
	'#type'			=> 'checkbox',
	'#title'		=> 'Enable submission method #1 (original)',
); 

$form['enable_submission_2'] = array(
	'#type'			=> 'checkbox',
	'#title'		=> 'Enable submission method #2 (list)',
); 

$form['enable_workflow execution'] = array(
	'#type'			=> 'checkbox',
	'#title'		=> 'Enable workflow/job execution',
);
 
$form["user_options"] = array(
	"#type" => "select", 
	"#title" => t("Select a type of input sequence"), 
	"#default_value" => variable_get("set of protein sequences", true),
	"#options" => array(
		"option1" => t("proteome"),
		"option2" => t("transcriptome"), 
		"option3" => t("set of protein sequences"),
	)      
);
 
$form['ProteomeFile'] = array(
	'#type'         => 'managed_file',
	'#title'        => t('Upload a proteome'),
	'#description'  => t('Sequences in a proteome will be directly sent to OrthoFinder for analyses.'),
	'#upload_validators' => array(
		'file_validate_extensions' => array('fasta fa faa'),
	),
	'#states' => array(
		'visible' => array(
			':input[name="user_options"]' => array('value' => 'option1'),
		),
	),
);
 
$form['TranscriptomeFile'] = array(
	'#type'         => 'managed_file',
	'#title'        => t('Upload a transcriptome'),
	'#description'  => t('Sequences in a transcriptome will be frame selected, translated, and filtered for proteins less than 100 amino acids.'),
	'#upload_validators' => array(
		'file_validate_extensions' => array('fasta fa fnn'),
	),
	'#states' => array(
		'visible' => array(
			':input[name="user_options"]' => array('value' => 'option2'),
		),
	),
);
 
$form["QueryPaste"] = array(
	"#type" => "textarea",
	"#title" => t('Paste sequences below'),
	"#required"   => false,
	'#states' => array(
		'visible' => array(
			array(
				array(':input[name="user_options"]' => array('value' => 'option3'),),
			),     
		),
	),  
);
  
$form['SelectDB'] = array (
	'#type'         => 'radios',
	'#options'      => array(
		'EntireDB'   => t('Select Entire Database'),
		'Select Species'      => t('Select Specific Species'),
	),
	'#prefix'           => '<h2>Subset Database or Select Entire Database</h2>',
);

/*
$databaseListProtein = OrthoqueryDiamondDatabases::getDatabasesSpecific("SELECT * FROM tseq_db_existing_locations WHERE type='Protein' and name like '%UniGene%' ORDER BY name ASC, version DESC");
$databaseList_toShow = array();
$databaseList_toShowProtein['default'] = t('--');
foreach($databaseListProtein as $dbList){
	$databaseList_toShowProtein[$dbList['db_id']] = $dbList['name'].', version '.$dbList['version'];
}
*/

$isg_species_fourletter_dir = "/isg/treegenes/treegenes_store/FTP/Unigenes/98/TreeGenes_UniGene/";
$isg_species_fourletter_arr = scandir($isg_species_fourletter_dir);
//dpm($isg_species_fourletter_arr);
$species_fourletter_arr = array();
foreach($isg_species_fourletter_arr as $item) {
	if(is_dir($isg_species_fourletter_dir . $item)) {
		if($item != '.' && $item != '..') {
			//dpm($item);
			//Perform SQL to determine which species based on the four letter code
			$results = chado_query('SELECT * FROM chado.organism LEFT JOIN chado.organismprop ON organism.organism_id = organismprop.organism_id WHERE value ILIKE :four_letter_code LIMIT 1;', array(
				':four_letter_code' => $item,
			));
			foreach($results as $row) {
				//dpm($row);
				//array_push($species_fourletter_arr, $item => $row->value);
				$species_fourletter_arr[$item] = $row->genus . " " . $row->species;
			}
			
		}
	}
}

$form['species_list']=array(
   '#type'=>'select',
   '#title' => t('Species'),
   //'#options' => $databaseList_toShowProtein,
   '#options' => $species_fourletter_arr,
   '#multiple' => true,
   //'#attributes'=>array('size'=>4),
   '#maxlength' => 50,
   '#maxheight' => 100,
   //'#weight'=>8,
   '#states' => array(
		'visible' => array(
			':input[name="SelectDB"]' => array('value' => 'Select Species'),
		),
	),
);
		 
$form['ParamsOrthoFinder'] = array(
  '#type'           => 'fieldset',
  '#title'          => 'Default Parameters for OrthoFinder Execution',
  '#collapsible'    => TRUE,
  '#collapsed'      => TRUE,
  '#states' => array(
		'visible' => array(
			array(
				array(':input[name="user_options"]' => array('value' => 'option1')), 
				'or',
				array(':input[name="user_options"]' => array('value' => 'option2')),
			 ),
		),
		'disabled' => array(
			array(
				array(':input[name="user_options"]' => array('value' => 'option3')),
			),
		),
	),  
);

$form['ParamsOrthoFinder']['Program'] = array(
	'#type'         =>  'textfield',
	'#title'        =>  'Sequence Similarity Search Program',
	'#size'         =>  '10',
	'#field_prefix'  =>  'Method of aligning protein sequences.<br />',
	'#required'     =>  true,
	'#default_value'      =>  t('Diamond'),
	'#disabled' => TRUE,
	'#attributes' => array('readonly' => 'readonly'),
);

$form['ParamsOrthoFinder']['Inflation'] = array(
	'#type'         =>  'textfield',
	'#title'        =>  'Inflation Parameter',
	'#size'         =>  '10',
	'#field_prefix'  =>  'Inflation parameter used to create orthogroups <br />',
	'#required'     =>  true,
	'#default_value'      =>  t('1.5'),
	'#disabled' => TRUE,
	'#attributes' => array('readonly' => 'readonly'),
);

$form['ParamsEggNOG'] = array(
  '#type'           => 'fieldset',
  '#title'          => 'Default Parameters for EggNOG-mapper Execution',
  '#collapsible'    => TRUE,
  '#collapsed'      => TRUE,
  '#states' => array(
		'visible' => array(
			array(
				array(':input[name="user_options"]' => array('value' => 'option3')), 
			 ),
		),
		'disabled' => array(
			array(
				array(':input[name="user_options"]' => array('value' => 'option1')),
				'or',
				array(':input[name="user_options"]' => array('value' => 'option2')),
			),
		),
	),  
);

$form['ParamsEggNOG']['targetCoverage'] = array(
	'#type'         =>  'textfield',
	'#title'        =>  'Target Coverage',
	'#size'         =>  '10',
	'#field_prefix'  =>  'Report only alignments above the given percentage of subject cover.<br />',
	'#required'     =>  true,
	'#default_value'      =>  t('70'),
);

$form['ParamsEggNOG']['queryCoverage'] = array(
	'#type'         =>  'textfield',
	'#title'        =>  'Query Coverage',
	'#size'         =>  '10',
	'#field_prefix'  =>  'Report only alignments above the given percentage of query cover.<br />',
	'#required'     =>  true,
	'#default_value'      =>  t('70'),
	'#disabled' => TRUE,
	'#attributes' => array('readonly' => 'readonly'),
);

$form['ParamsEggNOG']['maxTarget'] = array(
	'#type'         =>  'textfield',
	'#title'        =>  'Query Coverage',
	'#size'         =>  '10',
	'#field_prefix'  => 'Report a maximum number of alignments to a target sequence.<br />',
	'#required'     =>  true,
	'#default_value'      =>  t('3'),
	'#disabled' => TRUE,
	'#attributes' => array('readonly' => 'readonly'),
);

$form['ParamsEggNOG']['eValue'] = array(
	'#type'         =>  'textfield',
	'#title'        =>  'Query Coverage',
	'#size'         =>  '10',
	'#field_prefix'  => 'Specified E-value threshold.<br />',
	'#required'     =>  true,
	'#default_value'      =>  t('10e-5'),
	'#disabled' => TRUE,
	'#attributes' => array('readonly' => 'readonly'),
);


//Pasted Query:
if(!empty($form_state['values']['QueryPaste']))
{
	$nameFile = "/tmp/".date('YMd_His').'.fasta';
	$queryFile = file_put_contents($queryFile, $form_state['values']['QueryPaste']);
}


//Get information for uploaded files
//Query Upload (transcriptome)
if(!empty($form_state['values']['TranscriptomeFile']))
{
	$queryFile = file_load($form_state['values']['TranscriptomeFile']);
	if (is_object($queryFile))
	{
	  $queryFile = drupal_realpath($queryFile->uri);
	  if($debug){drupal_set_message("Query File: ".$queryFile);}
	}
	else
	{
		if($debug){drupal_set_message("Query File uplaod failed.");}
	}        
}

if(!empty($form_state['values']['ProteomeFile']))
{
	$queryFile = file_load($form_state['values']['ProteomeFile']);
	if (is_object($queryFile))
	{
	  $queryFile = drupal_realpath($queryFile->uri);
	  if($debug){drupal_set_message("Query File: ".$queryFile);}        
	}
	else
	{
		if($debug){drupal_set_message("Query File upload failed.");}
	}        
}



//send data to galaxy

/*
module_load_include('inc','tripal_galaxy','tripal_galaxy.api');
global $user;
$uid = $user->uid;
$server_options = array();
$rids = is_array($user->roles) ? array_keys($user->roles) : array();
$use_api="1e878bd3364e150a7a1107a0640b1125";
$records = db_query('SELECT galaxy_id, url, api_key, servername FROM {tripal_galaxy}');
	foreach ($records as $r) {
		$server_options[$r->url . ',' . $r->api_key] = $r->servername;
		// use blend4php to get list of Galaxy history entries
		$b4p_galaxy = tripal_galaxy_get_connection($r->galaxy_id);
		$b4p_history = new GalaxyHistories($b4p_galaxy);
		$history_name="OrthoQuery";
		$history=$b4p_history->create([
			'name'=> $history_name,
		]);
		// Now upload the file.
		if (is_object($queryFile))
		{
			tripal_galaxy_upload_file($b4p_galaxy, $queryFile->fid, $form_state['values']['history']);
		}
	}
*/

// Get the list of galaxies.
$galaxies = tripal_galaxy_get_galaxies();
// Iterate through that list to perform actions on each, or retrieve
// information.
$server_options = array();
//  krumo($galaxies);
foreach ($galaxies as $galaxy) {
	// Get the galaxy ID
	$galaxy_id = $galaxy->galaxy_id;

	// Do stuff here: add galaxy instance to the drop-down box
	$server_options[$galaxy_id] = $galaxy->servername;
}
$form['galaxy_server'] = array(
  '#type' => 'select',
  '#title' => 'Galaxy server',
  '#options' => $server_options,
  '#description' => count($server_options) ? '' : 'Set up a Galaxy instance at admin/tripal/extension/galaxy.',
  '#required' => TRUE,
);

//submit job  - execute galaxy workflows
$form['submit_button_1'] = array(
	'#type' => 'submit',
	'#value' => t('Submit'),
	//'#submit' => array('submit_button_1_submit'),
	'#states' => array(
		'visible' => array(
			//':input[name="SelectDB"]' => array('value' => 'EntireDB'),
		),
	), 
);
    
    
    
    
    