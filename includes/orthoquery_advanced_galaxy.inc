<?php
function orthoquery_advanced_invoke_proteome_workflow(&$form_state) {
	$debug = false;
	if ($form_state['values']['debug'] == 1) {
		$debug = true;
	}
	
	$do_only_test_dataset = false;
	if ($form_state['values']['do_only_test_dataset'] == 1) {
		$do_only_test_dataset = true;
	}
	
	$enable_submission = false;
	if ($form_state['values']['enable_submission'] == 1) {
		$enable_submission = true;
	}
	
	$enable_submission_2 = false;
	if ($form_state['values']['enable_submission_2'] == 1) {
		$enable_submission_2 = true;
	}	
	
	$enable_workflow_execution = false;
	if ($form_state['values']['enable_workflow_execution'] == 1) {
		$enable_workflow_execution = true;
	}	
	
	if($debug) {
		dpm($form_state['values']);
	}
	if(true) {
		// create file(s)
		global $user;
		$file_prefix = date('Ymd_His_') . $user->uid;
		// parameter file
		/*
		$file_name = 'public://' . $file_prefix . '_params.tsv';
		$data = 'HEADERS YES' . PHP_EOL;
		$data .= 'NUMVARENV 6' . PHP_EOL;
		$data .= 'NUMMARK 8' . PHP_EOL;
		$data .= 'NUMINDIV 6' . PHP_EOL;
		$data .= 'DIMMAX 2' . PHP_EOL;
		$data .= 'SAVETYPE END BEST 0.01' . PHP_EOL;
		$file_params = file_save_data($data, $file_name);  
		$fid = $file_params->fid;
		*/
		$selected_species_files = array();
		if($do_only_test_dataset != true) {
			//Check to see whether user selected EntireDB or Select Species
			$selected_species_count = 0;
		  
			if($form_state['values']['SelectDB'] == 'Select Species') {
				//check species_list
				$selected_species_count = count($form_state['value']['species_list']);
				$isg_treegenes_unigene_dir = "/isg/treegenes/treegenes_store/FTP/Unigenes/98/TreeGenes_UniGene/";
				foreach($form_state['values']['species_list'] as $species_fourletter) {
					//create the necessary files
					$temp_dir_arr = scandir($isg_treegenes_unigene_dir . $species_fourletter);
					foreach($temp_dir_arr as $filename) {
						if(stripos($filename,'_prots.fasta') !== FALSE && stripos($filename,'TG98_') !== FALSE) {
							//found a file that ends with _prots.fasta so add it to the array
							array_push($selected_species_files, $isg_treegenes_unigene_dir . $species_fourletter . '/' . $filename);
						}
					}
				}
				
				//Now perform the file operations for the files found
				$species_files_fid = array();
				$species_files_objects = array();
				foreach($selected_species_files as $file_location) {
				  $file_prefix = 'oqa_' . date('Ymd_His_') . $user->uid;
				  // parameter file
				  $file_name = 'public://' . $file_prefix . '_' . basename($file_location); 
				  dpm('Saving species file and creating file object for:' . $file_name);
				  $data = file_get_contents($file_location);
				  $file_object = file_save_data($data, $file_name); 
				  unset($data);
				  $fid = $file_object->fid;
				  file_usage_add($file_object, 'orthoquery_advanced', basename($file_location), $fid);

				  array_push($species_files_fid, $fid);
				  array_push($species_files_objects, $file_object);
				}
				
				if($debug) {
					dpm($species_files_fid);
					dpm($species_files_objects);
				}
			}
			else {
				//EntireDB
				//This isn't supported as yet
				drupal_set_message('EntireDB has been selected, this takes a bit longer since we have to load the whole file database, thank you for your patience');
				//check species_list
				$selected_species_count = count($form_state['value']['species_list']);
				$isg_treegenes_unigene_dir = "/isg/treegenes/treegenes_store/FTP/Unigenes/98/TreeGenes_UniGene/";
				$all_species_fourletter = scandir($isg_treegenes_unigene_dir);
				foreach($all_species_fourletter as $species_fourletter) {
					if($species_fourletter != '.' && $species_fourletter != '..' && is_dir($isg_treegenes_unigene_dir . $species_fourletter)) {
						//create the necessary files
						$temp_dir_arr = scandir($isg_treegenes_unigene_dir . $species_fourletter);
						foreach($temp_dir_arr as $filename) {
							if(stripos($filename,'_prots.fasta') !== FALSE && stripos($filename,'TG98_') !== FALSE) {
								//found a file that ends with _prots.fasta so add it to the array
								array_push($selected_species_files, $isg_treegenes_unigene_dir . $species_fourletter . '/' . $filename);
							}
						}
					}
				}

				//Now perform the file operations for the files found
				$species_files_fid = array();
				$species_files_objects = array();
				foreach($selected_species_files as $file_location) {
					$file_prefix = 'oqa_' . date('Ymd_His_') . $user->uid;
					// parameter file
					$file_name = 'public://' . $file_prefix . '_' . basename($file_location); 
					dpm('Saving species file and creating file object for:' . $file_name);
					$data = file_get_contents($file_location);
					$file_object = file_save_data($data, $file_name); 
					unset($data);
					$fid = $file_object->fid;
					file_usage_add($file_object, 'orthoquery_advanced', basename($file_location), $fid);

					array_push($species_files_fid, $fid);
					array_push($species_files_objects, $file_object);
				}

				if($debug) {
					dpm($species_files_fid);
					dpm($species_files_objects);
				}			  
			}	  		  
		}
		else {
			$isg_treegenes_unigene_dir = drupal_get_path('module', 'orthoquery_advanced') . '/example_dataset2/';
			$temp_dir_arr = scandir($isg_treegenes_unigene_dir);
			foreach($temp_dir_arr as $filename) {
				if(stripos($filename,'.faa') !== FALSE) {
					//found a file that ends with _prots.fasta so add it to the array
					array_push($selected_species_files, $isg_treegenes_unigene_dir . $filename);
				}
			}
			
			//Now perform the file operations for the files found
			$species_files_fid = array();
			$species_files_objects = array();
			foreach($selected_species_files as $file_location) {
			  $file_prefix = 'oqa_' . date('Ymd_His_') . $user->uid;
			  // parameter file
			  $file_name = 'public://' . $file_prefix . '_' . basename($file_location); 
			  if($debug) {
				dpm('Saving species file and creating file object for:' . $file_name);
			  }
			  $data = file_get_contents($file_location);
			  $file_object = file_save_data($data, $file_name); 
			  unset($data);
			  $fid = $file_object->fid;
			  file_usage_add($file_object, 'orthoquery_advanced', basename($file_location), $fid);

			  array_push($species_files_fid, $fid);
			  array_push($species_files_objects, $file_object);
			}
			
			if($debug) {				
				dpm($species_files_fid);
				dpm($species_files_objects);
			}			
		}

		if($do_only_test_dataset != true) {
			$file = file_load($form_state['values']['ProteomeFile']);
			if($debug) {
				dpm('File');
				dpm($file);
			}
			$file->status = FILE_STATUS_PERMANENT;
			$file_proteome = file_save($file); 
			$fid = $file_proteome->fid;
			// We don't want the file to disappear when Drupal performs it's cleaning so
			// we have to tell Drupal that the file is being used.  See the Drupal
			// documentation for the meaning of the input arguments.
			file_usage_add($file_proteome, 'orthoquery_advanced', 'proteome_file', $fid);
		}

		if($enable_submission == true) {
			// Connect to the Galaxy instance using the galaxy ID selected by the user
			$galaxy_id = $form_state['values']['galaxy_server'];
			$galaxy_conn = tripal_galaxy_get_connection($galaxy_id);
			// Check if the connection is valid connection
			if (!$galaxy_conn) {
				// retreive and display any connection errors
				$error = $galaxy->getError();
				drupal_set_message('Could not connect to Galaxy server. ' . $error['message'], 'error');
			}

			// Test if the Galaxy insteance is available
			$is_connected = tripal_galaxy_test_connection(['galaxy_id' => $galaxy_id]);
			if ($is_connected) {
				// Prepare the workflow for execution
				// Provide the values to uniquely find the workflow.
				$values = array(
				  'workflow_id' => 'bf60fd5f5f7f44bf', //this is for treegenes/treegenes_store/FTP/Unigenes/98/TreeGenes_UniGene/
				  //'workflow_id' => '33b43b4e7093c91f', //this is for Risharde's infrastructure @home
				  'galaxy_id' => $galaxy_id,
				);
				// Find the workflow using the values. This function always returns an
				// array of workflows that match the criteria.  By providing the workflow_id
				// and the galaxy_id it should only ever match one workflow.
				$workflows = tripal_galaxy_get_workflows($values);
				if($debug) {
					dpm('Workflows');
					dpm($workflows);
				}
				$workflow = $workflows[0];
				// We can now initialize the workflow submission.
				$sid = tripal_galaxy_init_submission($workflow, $user);
				if($debug) {
					dpm('SID');
					dpm($sid);
				}
				// Retrieve the $submission object using a known submission ID.
				$submission = tripal_galaxy_get_submission($sid);

				// Get the history name.
				$history_name = tripal_galaxy_get_history_name($submission);
				$history = tripal_galaxy_create_history($galaxy_conn, $history_name);
				if (!$history) {
					$error = $galaxy->getError();
					throw new Exception($error['message']);
				}
				// Get the history contents so we don't upload the same file more than once
				// in the event that this invocation occurs more than once.
				$ghistory_contents = new GalaxyHistoryContents($galaxy_conn);
				$history_contents = $ghistory_contents->index(array('history_id' => $history['id']));
				if ($history_contents === False) {
				  $error = $galaxy->getError();
				  dpm($error);
				  throw new Exception($error['message']);
				}


				$galaxy_files = array();

				if ($do_only_test_dataset != true) {
					// Upload the files to Galaxy.
					$galaxy_proteome_file = tripal_galaxy_upload_file($galaxy_conn, $file_proteome->fid, $history['id'], $history_contents);
					//Add it to the galaxy_files array
					array_push($galaxy_files, $galaxy_proteome_file);
				}

				//Cater for selected species files
				$f_count = count($species_files_fid);

				for($i=0; $i<$f_count; $i++) {
					$file = tripal_galaxy_upload_file($galaxy_conn, $species_files_fid[$i], $history['id'], $history_contents);
					array_push($galaxy_files, $file);
				}
				//dpm($species_files_fid);
				//dpm($species_files_objects);

				// Invoke the workflow
				/*
				$inputs = array(
				  0 => array(
					'id' => $galaxy_proteome_file['id'],
					'src' => $galaxy_proteome_file['hda_ldda'],
				  ),
				);
				*/
				
				//Generate the inputs array
				$inputs = array();
				//array_push($inputs, array());
				$f_count = count($galaxy_files);
				for($i=0; $i<$f_count; $i++) {
					array_push($inputs, 
						array(
							'id' => $galaxy_files[$i]['id'], 
							'src' => $galaxy_files[$i]['hda_ldda'],
						)
					);
				}
				
				if($debug) {
					dpm($inputs);
				}
				
				$parameters = tripal_galaxy_get_workflow_defaults($galaxy_conn, $workflow->workflow_id);
				if($debug) {
					dpm($parameters);
				}
				
				
				$submission = tripal_galaxy_get_submission($sid);
				
				if($enable_workflow_execution) {
					tripal_galaxy_invoke_workflow($galaxy_conn, $submission, $parameters, $inputs, $history);
				}
			}
		}
		
		if($enable_submission_2 == true) {
			// Connect to the Galaxy instance using the galaxy ID selected by the user
			$galaxy_id = $form_state['values']['galaxy_server'];
			$galaxy_conn = tripal_galaxy_get_connection($galaxy_id);
			// Check if the connection is valid connection
			if (!$galaxy_conn) {
				// retreive and display any connection errors
				$error = $galaxy->getError();
				drupal_set_message('Could not connect to Galaxy server. ' . $error['message'], 'error');
			}

			// Test if the Galaxy insteance is available
			$is_connected = tripal_galaxy_test_connection(['galaxy_id' => $galaxy_id]);
			if ($is_connected) {
				// Prepare the workflow for execution
				// Provide the values to uniquely find the workflow.
				$values = array(
				  'workflow_id' => 'bf60fd5f5f7f44bf', //this is for treegenes/treegenes_store/FTP/Unigenes/98/TreeGenes_UniGene/
				  //'workflow_id' => '33b43b4e7093c91f', //this is for Risharde's infrastructure @home
				  'galaxy_id' => $galaxy_id,
				);
				// Find the workflow using the values. This function always returns an
				// array of workflows that match the criteria.  By providing the workflow_id
				// and the galaxy_id it should only ever match one workflow.
				$workflows = tripal_galaxy_get_workflows($values);
				if($debug) {
					dpm('Workflows');
					dpm($workflows);
				}
				$workflow = $workflows[0];
				// We can now initialize the workflow submission.
				$sid = tripal_galaxy_init_submission($workflow, $user);
				if($debug) {
					dpm('SID');
					dpm($sid);
				}
				// Retrieve the $submission object using a known submission ID.
				$submission = tripal_galaxy_get_submission($sid);

				// Get the history name.
				$history_name = tripal_galaxy_get_history_name($submission);
				$history = tripal_galaxy_create_history($galaxy_conn, $history_name);
				if (!$history) {
					$error = $galaxy->getError();
					throw new Exception($error['message']);
				}
				// Get the history contents so we don't upload the same file more than once
				// in the event that this invocation occurs more than once.
				$ghistory_contents = new GalaxyHistoryContents($galaxy_conn);
				$history_contents = $ghistory_contents->index(array('history_id' => $history['id']));
				if ($history_contents === False) {
				  $error = $galaxy->getError();
				  dpm($error);
				  throw new Exception($error['message']);
				}


				$galaxy_files = array();

				if ($do_only_test_dataset != true) {
					// Upload the files to Galaxy.
					$galaxy_proteome_file = tripal_galaxy_upload_file($galaxy_conn, $file_proteome->fid, $history['id'], $history_contents);
					//Add it to the galaxy_files array
					array_push($galaxy_files, $galaxy_proteome_file);
				}

				//Cater for selected species files
				$f_count = count($species_files_fid);

				for($i=0; $i<$f_count; $i++) {
					$file = tripal_galaxy_upload_file($galaxy_conn, $species_files_fid[$i], $history['id'], $history_contents);
					array_push($galaxy_files, $file);
				}
				//dpm($species_files_fid);
				//dpm($species_files_objects);

				// Invoke the workflow
				/*
				$inputs = array(
				  0 => array(
					'id' => $galaxy_proteome_file['id'],
					'src' => $galaxy_proteome_file['hda_ldda'],
				  ),
				);
				*/
				
				//Generate the inputs array
				$inputs = array();
				//array_push($inputs, array());
				$f_count = count($galaxy_files);
				for($i=0; $i<$f_count; $i++) {
					array_push($inputs, 
						array(
							'id' => $galaxy_files[$i]['id'],
							'name' => $galaxy_files[$i]['name'],
							'src' => $galaxy_files[$i]['hda_ldda'],
						)
					);
				}
				
				if($debug) {
					//dpm($inputs);
				}
				
				
				// Create the array containing details about the dataset 
				// collection you want to create on Galaxy.
				$dataset_name = 'my_collection_' . $history['id'];
				$ds_details = [
					 'collection_type' => 'list',
					 'instance_type' => 'history',
					 'name' => $dataset_name,
					 'history_id' => $history['id'],
					 'element_identifiers' => $inputs
				];

				if($debug) {
					//dpm($inputs);
					dpm('DS_DETAILS VARIABLE');
					dpm($ds_details);
				}

				// Now create the dataset collection
				$gds_collection = new GalaxyDatasetCollections($galaxy);
				if($debug) {
					//dpm($inputs);
					dpm('GDS_COLLECTION VARIABLE:');
					dpm($gds_collection);
				}					
				
				$ds_collection = $gds_collection->create($ds_details);
				if (!$ds_collection) {
				   $error = $galaxy->getError();
				   throw new Exception($error['message']);
				}	
				
			
				
				if($debug) {
					//dpm($inputs);
					dpm('DS_COLLECTION VARIABLE:');
					dpm($ds_collection);
				}
				
				
				$parameters = tripal_galaxy_get_workflow_defaults($galaxy_conn, $workflow->workflow_id);
				if($debug) {
					dpm('PARAMETERS VARIABLE:');
					dpm($parameters);
				}
				
				
				$submission = tripal_galaxy_get_submission($sid);
				
				if($enable_workflow_execution) {
					tripal_galaxy_invoke_workflow($galaxy_conn, $submission, $parameters, $ds_collection, $history);
				}
			}
		}		
	}
}

?>